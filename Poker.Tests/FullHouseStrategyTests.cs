﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;
using Poker.Strategies.Interfaces;

namespace Poker.Tests
{
    [TestClass]
    public class FullHouseStrategyTests
    {
        [TestMethod]
        public void Evaluate_NotFullHouse_ShouldReturnFalse()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.ThreeOfAKind]);
            IThreeOfAKindStrategy strategy = new ThreeOfAKindStrategy(hand);
            var testScope = new FullHouseStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);

        }

        [TestMethod]
        public void Evaluate_TwoPair_ShouldReturnFalse()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.TwoPair]);
            IThreeOfAKindStrategy strategy = new ThreeOfAKindStrategy(hand);
            var testScope = new FullHouseStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);

        }

        [TestMethod]
        public void Evaluate_FullHouse_ShouldReturnTrue()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.FullHouse]);
            IThreeOfAKindStrategy strategy = new ThreeOfAKindStrategy(hand);
            var testScope = new FullHouseStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);

        }
    }
}
