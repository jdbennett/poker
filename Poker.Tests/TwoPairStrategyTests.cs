﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;
using Poker.Strategies.Interfaces;

namespace Poker.Tests
{
    [TestClass]
    public class TwoPairStrategyTests
    {
        [TestMethod]
        public void Evaluate_FullHouse_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.FullHouse]);
            IPairStrategy strategy = new PairStrategy(hand);
            IThreeOfAKindStrategy threeOfAKindStrategy = new ThreeOfAKindStrategy(hand);
            var testScope = new TwoPairStrategy(hand, strategy, threeOfAKindStrategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_TwoPair_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.TwoPair]);
            IPairStrategy strategy = new PairStrategy(hand);
            IThreeOfAKindStrategy threeOfAKindStrategy = new ThreeOfAKindStrategy(hand);
            var testScope = new TwoPairStrategy(hand, strategy, threeOfAKindStrategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Evaluate_ThreeOfAKind_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.ThreeOfAKind]);
            IPairStrategy strategy = new PairStrategy(hand);
            IThreeOfAKindStrategy threeOfAKindStrategy = new ThreeOfAKindStrategy(hand);
            var testScope = new TwoPairStrategy(hand, strategy, threeOfAKindStrategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_FourOfAKind_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.FourOfAKind]);
            IPairStrategy strategy = new PairStrategy(hand);
            IThreeOfAKindStrategy threeOfAKindStrategy = new ThreeOfAKindStrategy(hand);
            var testScope = new TwoPairStrategy(hand, strategy, threeOfAKindStrategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }
    }
}
