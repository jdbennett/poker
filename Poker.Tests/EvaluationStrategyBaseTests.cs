﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;

namespace Poker.Tests
{
    [TestClass]
    public class EvaluationStrategyBaseTests
    {

        [TestMethod]
        public void GetSuitCount_AllSuits_ShouldReturn4()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", "6H 7D KC QD AS");

            //act
            var result = EvaluationStrategyBase.GetSuitCount(hand);

            //assert
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void GetSuitCount_OneSuit_ShouldReturn1()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", "6D 7D KD QD AD");

            //act
            var result = EvaluationStrategyBase.GetSuitCount(hand);

            //assert
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void HasValidStraight_NotAStraight_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", "AS 6H 7D 8S KC");

            //act
            var result = EvaluationStrategyBase.HasStraight(hand);

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasValidStraight_ValidStraight_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", Util.TestData[HandTypes.Straight]);

            //act
            var result = EvaluationStrategyBase.HasStraight(hand);

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void HasValidStraight_MissingNumber_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", "2S 7S 4S 5S 6C");

            //act
            var result = EvaluationStrategyBase.HasStraight(hand);

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasValidStraight_DuplicateNumber_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", "2S 3S 3H 5S 6C");

            //act
            var result = EvaluationStrategyBase.HasStraight(hand);

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasRoyalStraight_MissingAce_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("Test", "9S TS JS QS KC");

            //act
            var result = EvaluationStrategyBase.HasRoyalStraight(hand);

            //assert
            Assert.IsFalse(result);
        }


    }
}
