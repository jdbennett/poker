﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;
using Poker.Strategies.Interfaces;

namespace Poker.Tests
{
    [TestClass]
    public class RoyalFlushStrategyTests
    {
        [TestMethod]
        public void Evaluate_NotARoyalFlush_ShouldReturnFalse()
        {
            //arrange 
            PlayingHand hand = new PlayingHand("Test", Util.TestData[HandTypes.Straight]);
            IStraightStrategy straightStrategy = new StraightStrategy(hand);
            StraightFlushStrategy strategy = new StraightFlushStrategy(hand, straightStrategy);
            var testScope = new RoyalFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_StraightNotARoyalFlush_ShouldReturnFalse()
        {
            //arrange 
            PlayingHand hand = new PlayingHand("Test", Util.TestData[HandTypes.StraightFlush]);
            IStraightStrategy straightStrategy = new StraightStrategy(hand);
            StraightFlushStrategy strategy = new StraightFlushStrategy(hand, straightStrategy);
            var testScope = new RoyalFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_RoyalFlush_ShouldReturnTrue()
        {
            //arrange 
            PlayingHand hand = new PlayingHand("Test", Util.TestData[HandTypes.RoyalFlush]);
            IStraightStrategy straightStrategy = new StraightStrategy(hand);
            StraightFlushStrategy strategy = new StraightFlushStrategy(hand, straightStrategy);
            var testScope = new RoyalFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void Evaluate_NotRoyalFlushMissingAce_ShouldReturnTrue()
        {
            //arrange 
            PlayingHand hand = new PlayingHand("Test", "TH JH QH KH 8H");
            IStraightStrategy straightStrategy = new StraightStrategy(hand);
            StraightFlushStrategy strategy = new StraightFlushStrategy(hand, straightStrategy);
            var testScope = new RoyalFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }
    }
}
