﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Poker.Tests
{
    [TestClass]
    public class EvaluatorTests
    {
        [TestMethod]
        public void Winnner_LeftHandRoyalFlush_ShouldReturnLeft()
        {
            //arrange
            PlayingHand left = new PlayingHand("LEFT", Util.TestData[HandTypes.RoyalFlush]);
            PlayingHand right = new PlayingHand("RIGHT", Util.TestData[HandTypes.StraightFlush]);
            var testScope = new Evaluator(new List<PlayingHand>() {left, right});

            //act
            var result = testScope.Winner();

            //assert
            Assert.AreEqual("LEFT", result);

        }

        [TestMethod]
        public void Winnner_BothRoyalFlush_ShouldReturnTie()
        {
            //arrange
            PlayingHand left = new PlayingHand("LEFT", Util.TestData[HandTypes.RoyalFlush]);
            PlayingHand right = new PlayingHand("RIGHT", Util.TestData[HandTypes.RoyalFlush]);
            var testScope = new Evaluator(new List<PlayingHand>() { left, right });

            //act
            var result = testScope.Winner();

            //assert
            Assert.AreEqual("TIE", result);

        }

        [TestMethod]
        public void Winnner_StraightFlush_ShouldReturnLeft()
        {
            //arrange
            PlayingHand left = new PlayingHand("LEFT", Util.TestData[HandTypes.StraightFlush]);
            PlayingHand right = new PlayingHand("RIGHT", Util.TestData[HandTypes.Pair]);
            var testScope = new Evaluator(new List<PlayingHand>() { left, right });

            //act
            var result = testScope.Winner();

            //assert
            Assert.AreEqual("LEFT", result);

        }
    }
}
