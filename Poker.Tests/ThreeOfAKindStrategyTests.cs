﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;

namespace Poker.Tests
{
    [TestClass]
    public class ThreeOfAKindStrategyTests
    {
        [TestMethod]
        public void Evaluate_FullHouse_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.FullHouse]);
            var testScope = new ThreeOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Evaluate_Straight_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.Pair]);
            var testScope = new ThreeOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_ThreeOfAKind_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.ThreeOfAKind]);
            var testScope = new ThreeOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }
    }
}
