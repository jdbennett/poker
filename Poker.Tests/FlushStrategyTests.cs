﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;

namespace Poker.Tests
{
    [TestClass]
    public class FlushStrategyTests 
    {
        [TestMethod]
        public void Evaluate_StraightFlush_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.StraightFlush]);
            var testScope = new FlushStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Evaluate_Straight_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.Straight]);
            var testScope = new FlushStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_RoyalFlush_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.RoyalFlush]);
            var testScope = new FlushStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }
    }
}
