﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Poker.Tests
{
    [TestClass]
    public class UtilTests
    {
        [TestMethod]
        public void ValidateInput_MissingCard_ShouldReturnFalse()
        {
            //arrange
            string input = "6H 7D 8S KC 9S QH JD 5H 3D";

            //act
            var result = Util.ValidateInput(input);

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateInput_ValidInput_ShouldReturnTrue()
        {
            //arrange
            string input = "AS 6H 7D 8S KC 9S QH JD 5H 3D";

            //act
            var result = Util.ValidateInput(input);

            //assert
            Assert.IsTrue(result);
        }


    }
}
