﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;
using Poker.Strategies.Interfaces;

namespace Poker.Tests
{
    [TestClass]
    public class StraightFlushStrategyTests
    {

        [TestMethod]
        public void Evaluate_NotAStraightFlush_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.TwoPair]);
            IStraightStrategy strategy = new StraightStrategy(hand);
            var testScope = new StraightFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_FlushNotStraight_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.Flush]);
            IStraightStrategy strategy = new StraightStrategy(hand);
            var testScope = new StraightFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_StraightNotFlush_ShouldReturnFalse()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.Straight]);
            IStraightStrategy strategy = new StraightStrategy(hand);
            var testScope = new StraightFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_StraightFlush_ShouldReturnTrue()
        {
            //arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.StraightFlush]);
            IStraightStrategy strategy = new StraightStrategy(hand);
            var testScope = new StraightFlushStrategy(hand, strategy);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }

    }
}
