﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker.Strategies;

namespace Poker.Tests
{
    [TestClass]
    public class FourOfAKindStrategyTests
    {
        [TestMethod]
        public void Evaluate_NotFourOfAKind_ShouldReturnFalse()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.Flush]);
            var testScope = new FourOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_TwoPair_ShouldReturnFalse()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.TwoPair]);
            var testScope = new FourOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Evaluate_ThreeOfAKind_ShouldReturnFalse()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.ThreeOfAKind]);
            var testScope = new FourOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void Evaluate_FourOfAKind_ShouldReturnTrue()
        {
            //Arrange
            PlayingHand hand = new PlayingHand("test", Util.TestData[HandTypes.FourOfAKind]);
            var testScope = new FourOfAKindStrategy(hand);

            //act
            var result = testScope.Evaluate();

            //assert
            Assert.IsTrue(result);
        }
    }
}
