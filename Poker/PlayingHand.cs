﻿using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class PlayingHand
    {
        public readonly List<PlayingCard> PlayingCards;
        public readonly string Name;

        /// <summary>
        /// Hand of cards.
        /// </summary>
        /// <param name="handName">UniqueName of the hand</param>
        /// <param name="hand">string input in the form of 
        /// {[Card][Suit] [Card][Suit] [Card][Suit] [Card][Suit] }[Card][Suit]}
        /// Ex:  6H 7D QH JC KS</param>
        public PlayingHand(string handName, string hand)
        {
            Name = handName;
            PlayingCards = new List<PlayingCard>();
            SetHand(hand);
        }

        private void SetHand(string hand)
        {
            List<string> cards = hand.Split(' ').ToArray().ToList();
            cards.ForEach(card =>
            {
                int cardval = Util.CardValues[card.Substring(0, 1)];
                Suit suit = Util.CardSuits[card.Substring(1, 1)];
                PlayingCards.Add(new PlayingCard(cardval, suit));
            });
        }

    }
}
