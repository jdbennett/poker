﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Poker
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter the hands:");
            string line = Console.ReadLine();

            if (Util.ValidateInput(line))
            {
                //create a new playing hand - using the first 14 characters
                //10 for the cards and 4 spaces
                var leftHand = new PlayingHand("LEFT", line.Left(14));
                var rightHand = new PlayingHand("RIGHT", line.Right(14));

                //get a new evaluator instance
                var evaluator = new Evaluator(new List<PlayingHand>() { leftHand, rightHand });

                //output the evaluation results
                Console.WriteLine(evaluator.Winner());
            }

            Console.ReadLine();


        }
    }
}
