﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Poker.Strategies.Interfaces;

namespace Poker
{
    public class Evaluator
    {
        private readonly List<PlayingHand> _hands;

        public Evaluator(List<PlayingHand> hands)
        {
            _hands = hands;
        }


        public string Winner()
        {
            Dictionary<string, HandTypes> results = new Dictionary<string, HandTypes>();

            _hands.ForEach(hand =>
            {
                var strategies = Util.GetStrategies(hand);
                results.Add(hand.Name, GetHandType(strategies));

            });

            return DetermineWinner(results);
        }

        private string DetermineWinner(Dictionary<string, HandTypes> results)
        {
            HandTypes highestHand = results.Select(hands => hands.Value).OrderBy(c => c).First();

            int highestHandCount = results.Count(hands => hands.Value == highestHand);

            if (highestHandCount == 1)
                return results.FirstOrDefault(hand => hand.Value == highestHand).Key;

            return "TIE";
        }

        private HandTypes GetHandType(List<IEvaluationStrategy> strategies)
        {
            //Order them first - RoyalFlush, StraightFlush, etc.
            strategies = strategies.OrderBy(st => (int) st.HandType).ToList();

            HandTypes result = HandTypes.RoyalFlush;

            foreach (var strategy in strategies)
            {
                result = strategy.HandType;
                if(strategy.Evaluate())
                    break;
            }

            return result;
        }
    }
}
