﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Poker.Strategies;
using Poker.Strategies.Interfaces;

namespace Poker
{
    public static class Util
    {
        public static readonly Dictionary<string, int> CardValues = new Dictionary<string, int>()
        {
            {"2", 2},
            {"3", 3},
            {"4", 4},
            {"5", 5},
            {"6", 6},
            {"7", 7},
            {"8", 8},
            {"9", 9},
            {"T", 10},
            {"J", 11},
            {"Q", 12},
            {"K", 13},
            {"A", 14}
        };

        public static readonly Dictionary<string, Suit> CardSuits = new Dictionary<string, Suit>()
        {
            {"S", Suit.Spades},
            {"D", Suit.Diamonds},
            {"C", Suit.Clubs},
            {"H", Suit.Hearts}
        };

        public static readonly Dictionary<int, int> ValidStraights = new Dictionary<int, int>()
        {
            {2, 20},
            {3, 25},
            {4, 30},
            {5, 35},
            {6, 40},
            {7, 45},
            {8, 50},
            {9, 55},
            {10, 60}
        };

        public static readonly Dictionary<HandTypes, string> TestData = new Dictionary<HandTypes, string>()
        {
            {HandTypes.RoyalFlush, "TH JH QH KH AH"},
            {HandTypes.StraightFlush, "2D 3D 4D 5D 6D"},
            {HandTypes.FourOfAKind, "AS AD AH AC 2C"},
            {HandTypes.FullHouse, "3H 3D 3C 2D 2H"},
            {HandTypes.Flush, "2H 7H TH JH 5H"},
            {HandTypes.Straight, "2D 3C 4S 5H 6D"},
            {HandTypes.ThreeOfAKind, "2D 2S 2H 7C TH"},
            {HandTypes.TwoPair, "2D 2C 3D 3C TH"},
            {HandTypes.Pair, "2H 2C 5D 7S AH"}
        };

        public static string Right(this string val, int start)
        {
            return val.Substring(val.Length - start);
        }
        public static string Left(this string val, int length)
        {
            return val.Substring(0, 14);
        }
        public static bool ValidateInput(string val)
        {
            val = val.ToLower().Trim();
            //load the input into an array
            List<string> inputCards = val.Split(' ').ToList();

            //check for 10 cards
            if (inputCards?.Count != 10)
                return false;

            //hand validation 

            //put the valid characters into a string to use for comparison
            StringBuilder validationString = new StringBuilder();
            CardSuits.ToList().ForEach(suit => validationString.Append(suit.Key.ToLower()));
            CardValues.ToList().ForEach(card => validationString.Append(card.Key.ToLower()));

            //create arrays to compary
            List<char> validationArray = validationString.ToString().ToCharArray().ToList();
            List<char> inputArray = val.ToCharArray().ToList();
            inputArray = inputArray.Where(x => x != '\0').ToList();

            //invalid characters
            var invalid = inputArray.Except(validationArray).ToList();

            //if invalid had any return false
            //if (invalid.Any())
            //    return false;

            return true;

        }
        public static bool IsAny<T>(this List<T> collection)
        {
            if (collection == null)
            {
                return false;
            }

            return collection.Any();
        }

        //normally we would use an IoC component such as AutoFac to set this up
        //this will suffice for the exercise
        public static List<IEvaluationStrategy> GetStrategies(PlayingHand hand)
        {
            IPairStrategy pairStrategy = new PairStrategy(hand);
            IThreeOfAKindStrategy threeOfAkind = new ThreeOfAKindStrategy(hand);
            ITwoPairStrategy twoPairStrategy = new TwoPairStrategy(hand, pairStrategy, threeOfAkind);
            IFourOfAKindStrategy fourOfAKindStrategy = new FourOfAKindStrategy(hand);
            IStraightStrategy straightStrategy = new StraightStrategy(hand);
            IFlushStrategy flushStrategy = new FlushStrategy(hand);
            IFullHouseStrategy fullHouseStrategy = new FullHouseStrategy(hand, threeOfAkind);
            IStraightFlushStrategy straightFlushStrategy = new StraightFlushStrategy(hand, straightStrategy);
            IRoyalFlushStrategy royalFlushStrategy = new RoyalFlushStrategy(hand, straightFlushStrategy);

            return new List<IEvaluationStrategy>()
            {
                {pairStrategy},
                {threeOfAkind},
                {twoPairStrategy},
                {fourOfAKindStrategy},
                {straightStrategy},
                {flushStrategy},
                {fullHouseStrategy},
                {straightFlushStrategy},
                {royalFlushStrategy}
            };

        }

    }
}
