﻿using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class RoyalFlushStrategy : EvaluationStrategyBase, IRoyalFlushStrategy
    {
        private readonly IStraightFlushStrategy _straightFlushStrategy;

        public RoyalFlushStrategy(PlayingHand hand, IStraightFlushStrategy straightFlushStrategy) 
            : base(hand, HandTypes.RoyalFlush)
        {
            _straightFlushStrategy = straightFlushStrategy;
        }

        public bool Evaluate()
        {
            return _straightFlushStrategy.Evaluate() && HasRoyalStraight(Hand);
        }
        

    }
}
