﻿using System;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class FullHouseStrategy : EvaluationStrategyBase, IFullHouseStrategy
    {
        private readonly IThreeOfAKindStrategy _threeOfAKindStrategy;

        public FullHouseStrategy(PlayingHand hand, IThreeOfAKindStrategy threeOfAKindStrategy) 
            : base(hand, HandTypes.FullHouse)
        {
            _threeOfAKindStrategy = threeOfAKindStrategy;
        }

        public bool Evaluate()
        {
            var cardRanks = GetHandRanks(Hand);

            //can be only two types of cards
            if (cardRanks.Count != 2)
                return false;

            //check if one of them is three of a kind
            return _threeOfAKindStrategy.Evaluate();

        }
    }
}
