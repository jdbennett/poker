﻿namespace Poker.Strategies.Interfaces
{
    public interface IEvaluationStrategy
    {
        HandTypes HandType { get; }
        bool Evaluate();
    }
}
