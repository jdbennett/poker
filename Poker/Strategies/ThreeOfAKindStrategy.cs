﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class ThreeOfAKindStrategy : EvaluationStrategyBase, IThreeOfAKindStrategy
    {
        public ThreeOfAKindStrategy(PlayingHand hand) 
            : base(hand, HandTypes.ThreeOfAKind)
        {
        }

        public bool Evaluate()
        {
            bool result = false;

            List<int> cardRanks = GetHandRanks(Hand);

            cardRanks.ForEach(rank =>
            {
                int rankCount = Hand.PlayingCards.Count(card => card.CardValue == rank);
                if (rankCount == 3)
                    result = true;
            });

            return result;
        }
    }
}
