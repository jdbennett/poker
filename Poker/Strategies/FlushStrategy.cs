﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class FlushStrategy : EvaluationStrategyBase, IFlushStrategy
    {
        public FlushStrategy(PlayingHand hand) 
            : base(hand, HandTypes.Flush)
        {
        }

        public bool Evaluate()
        {
            if (GetSuitCount(Hand) > 1)
                return false;

            return true;
        }
    }
}
