﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class StraightStrategy : EvaluationStrategyBase, IStraightStrategy
    {
        public StraightStrategy(PlayingHand hand) 
            : base(hand, HandTypes.Straight)
        {
        }

        public bool Evaluate()
        {
            return HasStraight(Hand);
        }
    }
}
