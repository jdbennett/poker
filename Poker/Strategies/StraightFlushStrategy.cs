﻿using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class StraightFlushStrategy : EvaluationStrategyBase, IStraightFlushStrategy
    {
        private readonly IStraightStrategy _straightStrategy;

        public StraightFlushStrategy(PlayingHand hand, IStraightStrategy straightStrategy) 
            : base(hand, HandTypes.StraightFlush)
        {
            _straightStrategy = straightStrategy;
        }

        public bool Evaluate()
        {
            //not a straight flush
            if (GetSuitCount(Hand) > 1)
                return false;

            return _straightStrategy.Evaluate();
        }
    }
}
