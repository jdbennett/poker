﻿using System.Linq;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class FourOfAKindStrategy : EvaluationStrategyBase, IFourOfAKindStrategy
    {
        public FourOfAKindStrategy(PlayingHand hand) 
            : base(hand, HandTypes.FourOfAKind)
        {
        }

        public bool Evaluate()
        {
            bool result = false;

            //Get a list of distinct ranks (card values)
            var cardRanks = GetHandRanks(Hand);

            //for each one - see if any of them occur 4 times
            cardRanks.ForEach(rank =>
            {
                int rankCount = Hand.PlayingCards.Count(card => card.CardValue == rank);
                if (rankCount == 4)
                    result = true;
            });

            return result;
        }
    }
}
