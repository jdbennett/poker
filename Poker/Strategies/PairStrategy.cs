﻿using System.Collections.Generic;
using System.Linq;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class PairStrategy : EvaluationStrategyBase, IPairStrategy
    {
        public PairStrategy(PlayingHand hand) 
            : base(hand, HandTypes.Pair)
        {
        }

        public bool Evaluate()
        {
            bool result = false;

            List<int> cardRanks = GetHandRanks(Hand);

            cardRanks.ForEach(rank =>
            {
                int rankCount = Hand.PlayingCards.Count(card => card.CardValue == rank);
                if (rankCount == 2)
                    result = true;
            });
            return result;
        }
    }
}
