﻿using System.Collections.Generic;
using System.Linq;

namespace Poker.Strategies
{
    public abstract class EvaluationStrategyBase
    {
        protected PlayingHand Hand;
        public HandTypes HandType { get; }
        protected EvaluationStrategyBase(PlayingHand hand, HandTypes handType)
        {
            HandType = handType;
            Hand = hand;
        }

        public static int GetSuitCount(PlayingHand playingHand)
        {
            return playingHand.PlayingCards.Select(card => card.Suit).Distinct().Count();
        }

        //the idea here is the playing hand has already be checked for 5 cards
        //the first card value and the last card value are stored in a dictionary
        //sorting the playing hand by card value ascending - we select the first card value
        //and find the corresponding dictionary value for the key (first card).  if the sum
        // of the playing hand matches the dictionary value - it is a straight
        public static bool HasStraight(PlayingHand playingHand)
        {
            var firstCard = playingHand.PlayingCards.OrderBy(c => c.CardValue).First();
            int sum = playingHand.PlayingCards.Sum(card => card.CardValue);

            if (Util.ValidStraights[firstCard.CardValue] == sum)
                return true;

            return false;
        }

        public static bool HasRoyalStraight(PlayingHand playingHand)
        {
            //check if it is even a straight
            if (!HasStraight(playingHand))
                return false;

            //we know it is a straight - now just check to make sure the last card is an Ace
            if (playingHand.PlayingCards.OrderBy(card => card.CardValue).Last().CardValue == 14)
                return true;

            return false;
        }

        public static List<int> GetHandRanks(PlayingHand hand)
        {
            return hand.PlayingCards.Select(card => card.CardValue).Distinct().ToList();
        }
    }
}
