﻿using System;
using System.Collections.Generic;
using Poker.Strategies.Interfaces;

namespace Poker.Strategies
{
    public class TwoPairStrategy : EvaluationStrategyBase, ITwoPairStrategy
    {
        private readonly IPairStrategy _pairStrategy;
        private readonly IThreeOfAKindStrategy _threeOfAKindStrategy;
        public TwoPairStrategy(PlayingHand hand, IPairStrategy pairStrategy, 
            IThreeOfAKindStrategy threeOfAKindStrategy) 
            : base(hand, HandTypes.TwoPair)
        {
            _pairStrategy = pairStrategy;
            _threeOfAKindStrategy = threeOfAKindStrategy;
        }

        public bool Evaluate()
        {
            if (!_pairStrategy.Evaluate())
                return false;

            List<int> cardRanks = GetHandRanks(Hand);

            //four or more different cards - can't be two pair
            if (cardRanks.Count > 3)
                return false;

            //confired we have a pair and only 
            if (_threeOfAKindStrategy.Evaluate())
                return false;

            return true;

        }
    }
}
