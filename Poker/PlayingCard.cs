﻿namespace Poker
{
    public class PlayingCard
    {

        public readonly int CardValue;
        public readonly Suit Suit;

        public PlayingCard(int cardValue, Suit suit)
        {
            CardValue = cardValue;
            Suit = suit;
        }


    }
}
