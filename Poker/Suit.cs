﻿namespace Poker
{
    public enum Suit
    {
        Spades,
        Hearts,
        Diamonds,
        Clubs
    }
}
